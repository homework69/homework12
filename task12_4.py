"""
Создайте иерархию классов транспортных средств. В общем классе опишите общие для всех транспортных средств поля,
в наследниках – специфичные для них. Создайте несколько экземпляров.
Выведите информацию о каждом транспортном средстве.
"""


class Vehicles:
    def __init__(self, brand, model):
        self.brand = brand
        self.model = model

    def __str__(self):
        return f"{self.brand}, mod. {self.model}"


# Second level

class WheeledVehicles(Vehicles):
    def __init__(self, brand, model, count_of_wheels, traction_type):
        super().__init__(brand, model)
        self.count_of_wheels = count_of_wheels
        self.traction_type = traction_type

    def __str__(self):
        result = super().__str__()
        result += f", with {self.count_of_wheels} wheels on {self.traction_type} traction"
        return result


class WaterVehicles(Vehicles):
    def __init__(self, brand, model, see_or_river, surface_or_underwater):
        super().__init__(brand, model)
        self.see_or_river = see_or_river
        self.surface_or_underwater = surface_or_underwater

    def __str__(self):
        result = super().__str__()
        result += f", for {self.see_or_river}, is {self.surface_or_underwater}"
        return result


class RailwayVehicles(Vehicles):
    def __init__(self, brand, model, is_electric):
        super().__init__(brand, model)
        self.is_electric = is_electric

    def __str__(self):
        result = super().__str__()
        result += ", is electric " if self.is_electric else ""
        return result


class AirVehicles(Vehicles):
    def __init__(self, brand, model, lifting_type):
        super().__init__(brand, model)
        self.lifting_type = lifting_type

    def __str__(self):
        result = super().__str__()
        result += f",{self.lifting_type} lifting"
        return result


# Third level

class Car(WheeledVehicles):
    def __init__(self, brand, model, is_truck, count_of_wheels=4, traction_type='mechanical'):
        super().__init__(brand, model, count_of_wheels, traction_type)
        self.is_truck = is_truck

    def __str__(self):
        result = super().__str__()
        result = ("Truck " + result) if self.is_truck else ("Car " + result)
        return result


class Bus(WheeledVehicles):
    def __init__(self, brand, model, max_passengers):
        super().__init__(brand, model, 4, 'mechanical')
        self.max_passengers = max_passengers

    def __str__(self):
        result = super().__str__()
        result = "Bus " + result + f" for {self.max_passengers} passengers"
        return result


class Bicycle(WheeledVehicles):
    def __init__(self, brand, model, is_off_road):
        super().__init__(brand, model, 2, 'hand')
        self.is_off_road = is_off_road

    def __str__(self):
        result = super().__str__()
        result = "Bicycle " + result
        result += ", off-road" if self.is_off_road else ""
        return result


class Boat(WaterVehicles):
    def __init__(self, brand, model, see_or_river, is_sail):
        super().__init__(brand, model, see_or_river, 'surface')
        self.is_sail = is_sail

    def __str__(self):
        result = super().__str__()
        result = "Boat " + result
        result += ", have sail" if self.is_sail else ""
        return result


class Submarine(WaterVehicles):
    def __init__(self, brand, model, engine_type):
        super().__init__(brand, model, 'see', 'underwater')
        self.engine_type = engine_type

    def __str__(self):
        result = super().__str__()
        result = "Submarine " + result
        result += f", have {self.engine_type} engine"
        return result


class Plane(AirVehicles):
    def __init__(self, brand, model):
        super().__init__(brand, model, 'wings')

    def __str__(self):
        result = super().__str__()
        result = "Plane " + result
        return result


class Helicopter(AirVehicles):
    def __init__(self, brand, model):
        super().__init__(brand, model, 'propeller')

    def __str__(self):
        result = super().__str__()
        result = "Helicopter " + result
        return result


class Train(RailwayVehicles):
    def __init__(self, brand, model, is_electric, is_passengers):
        super().__init__(brand, model, is_electric)
        self.is_passengers = is_passengers

    def __str__(self):
        result = super().__str__()
        result = "Train " + result
        result += ", is passengers" if self.is_passengers else ""
        return result


if __name__ == '__main__':
    vehicles_list = [
        Car('Porshe', '911', False),
        Bus('Icarus', '', 52),
        Bicycle('Comanche', 'Ranger', True),
        Boat('Ракета-1', '340', 'river', False),
        Submarine('Запоріжжя', 'Б-435', 'diesel-electric'),
        Plane('Міг', '29'),
        Helicopter('Мі', '8'),
        Train('КВЗ', 'Т-1', True, True)
    ]

    print("\nList of vehicles: ")

    for elem in vehicles_list:
        elem_number = f"{vehicles_list.index(elem) + 1}. "
        print(elem_number + str(elem))
