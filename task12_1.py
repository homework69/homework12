"""
Создайте класс Editor, который содержит методы view_document и edit_document. Пусть метод edit_document выводит
на экран информацию о том, что редактирование документов недоступно для бесплатной версии.
Создайте подкласс ProEditor, в котором данный метод будет переопределён.
Введите с клавиатуры лицензионный ключ и, если он корректный, создайте экземпляр класса ProEditor, иначе Editor.
Вызовите методы просмотра и редактирования документов.
"""


class Editor:
    def __init__(self):
        self.__license_key = '123'

    def is_correct(self, license_key):
        return self.__license_key == license_key

    def view_document(self):
        print("Viewing the document")

    def edit_document(self):
        print("Document editing is not available in the free version!")


class ProEditor(Editor):

    def edit_document(self):
        print("Editing the  document")


license_check = input("Input license key: ")
editor1 = Editor()
if editor1.is_correct(license_check):
    editor1 = ProEditor()

editor1.view_document()
editor1.edit_document()
