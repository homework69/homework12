"""
Опишите классы графического объекта, прямоугольника и объекта, который может обрабатывать нажатия мыши.
Опишите класс кнопки. Создайте объект кнопки и обычного прямоугольника. Вызовите метод нажатия на кнопку.
"""


class GraphicObject:
    def __init__(self, name):
        self.name = name


class Rectangle:
    def __init__(self, width, length):
        self.width = width
        self.length = length


class MouseClik:
    def on_clik(self):
        print("The event of mouse clik")


class Button(GraphicObject, Rectangle, MouseClik):
    def __init__(self, name, width, length):
        GraphicObject.__init__(self, name)
        Rectangle.__init__(self, width, length)


button1 = Button('Button1', 2, 3)
# print(Button.__mro__)
rectangle1 = Rectangle(1, 4)
button1.on_clik()
