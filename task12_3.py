"""
Создайте иерархию классов с использованием множественного наследования.
Выведите на экран порядок разрешения методов для каждого из классов.
Объясните, почему линеаризации данных классов выглядят именно так.
"""


class A:
    pass


class B(A):
    pass


class C(A):
    pass


class D(B, C):
    pass


print("The result of the NW linearization algorithm:")
print(D.__mro__)
